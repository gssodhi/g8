<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$:5%*H}Rq820TXcpp4gsh[]%8]_i@1([CdIuj2~-~Rq;}l!3:vexU&N])%Pz%.#;');
define('SECURE_AUTH_KEY',  'k*wb,x#A0rA+==CDhZhd`3mSJZ3xH:bYX&l,7S9SAxcyN8bC[VKUo^QM(Y2JH)ts');
define('LOGGED_IN_KEY',    '+)jH7m[4dW).W-Wr369.R!0d?6vvMEf|El-3$^4LkP`.sD+oIz~m@gd5y*M|KtS<');
define('NONCE_KEY',        '>g.$f,n5DyFR0}oX9Sa$9QkzKW]fm$mAJWU@#`UMpQ9mjbxw4+bNVghj@m]>!dck');
define('AUTH_SALT',        'Eg7E{)0%o2H/Cr`*]NvP3ReIOuZZYAk]xaoS(FPx+KF,yisK)C%FuepED9R ms5=');
define('SECURE_AUTH_SALT', '|j#j1(C33iKGe(W{/4^H8?o=[+0Ya?mGvPmh|we<LQ,JnBxfO}`RS^C/4_P=kx95');
define('LOGGED_IN_SALT',   '4~<}=2<s(jV0GK9XQg 0i&zdFdPwf],B^F(jKetZn pB48Pid 7%S4XAiV^==e9e');
define('NONCE_SALT',       'TmT R(ej76{1g|a=XR?)2Ja4MR3ctj#+4vJI,S605%)Wvk3Fq-eR^&m%LFv/LCL}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'g8_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
