<?php
include_once("g8_con.php");
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			google.load("visualization", "1", {packages:["corechart"]});
			google.setOnLoadCallback(drawChart);
			function drawChart() {
				var data_column = google.visualization.arrayToDataTable([
					['Date', 'Visits'],
					<?php 
						$query = "SELECT count(ip) AS count, vdate FROM g8_google_chart GROUP BY vdate ORDER BY vdate";
						$exec = mysqli_query($con,$query);
						while($row = mysqli_fetch_array($exec)){
							echo "['".$row['vdate']."',".$row['count']."],";
						}
					?>
				]);
				var options_column = {title: 'Date wise visits', colors: ['green']};
				var chart_column = new google.visualization.ColumnChart(document.getElementById("columnchart"));
				chart_column.draw(data_column, options_column);
			}			
		</script>
	</head>
</html>