<?php
	include_once("g8_con.php");
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			google.load("visualization", "1", {packages:["geochart"]});
			google.setOnLoadCallback(drawRegionsMap);
			function drawRegionsMap() {
				var data_geo = google.visualization.arrayToDataTable([
					['Country', 'Visits'],
					<?php 
						$query = "SELECT count(ip) AS count, country FROM g8_google_chart GROUP BY country";
						$exec = mysqli_query($con,$query);
						while($row = mysqli_fetch_array($exec)){
							echo "['".$row['country']."',".$row['count']."],";
						}
					?>
				]);
				var options_geo = {colors: ['purple']};
				var chart_geo = new google.visualization.GeoChart(document.getElementById('geochart'));
				chart_geo.draw(data_geo, options_geo);
			}
		</script>
	</head>
</html>