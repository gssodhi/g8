<?php
include_once("g8_con.php");
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			google.load("visualization", "1", {packages:["corechart"]});
			google.setOnLoadCallback(drawChart);
			function drawChart() {
				var data_pie = google.visualization.arrayToDataTable([
 					['Browser', 'Visits'],
 					<?php 
 						$query = "SELECT count(ip) AS count, browser FROM g8_google_chart GROUP BY browser";
						$exec = mysqli_query($con,$query);
 						while($row = mysqli_fetch_array($exec)){
							echo "['".$row['browser']."',".$row['count']."],";
 						}
 					?>
 				]);
 				var options_pie = {title: 'Browser wise visits', colors: ['red','green','blue']};
				var chart_pie = new google.visualization.PieChart(document.getElementById('piechart'));
 				chart_pie.draw(data_pie, options_pie);
 			}
		</script>
	</head>
</html>