<?php
	include_once("lib/g8_google_chart_column_lib.php");
	include_once("lib/g8_google_chart_pie_lib.php");
	include_once("lib/g8_google_chart_geo_lib.php");
	include_once("lib/g8_google_chart_bar_lib.php");
?>
<html>
	<body>
		<h3>Column Chart</h3>
		<div id="columnchart" style="width: 900px; height: 500px;"></div>
		<h3>Pie Chart</h3>
 		<div id="piechart" style="width: 900px; height: 500px;"></div>
 		<h3>Geo Chart</h3>
		<div id="geochart" style="width: 900px; height: 500px;"></div>
		<h3>Bar Chart</h3>
		<div id="barchart" style="width: 900px; height: 500px;"></div>
	</body>
</html>